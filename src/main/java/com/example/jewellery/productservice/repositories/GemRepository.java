package com.example.jewellery.productservice.repositories;

import com.example.jewellery.productservice.entities.Category;
import com.example.jewellery.productservice.entities.Gem;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface GemRepository extends JpaRepository<Gem, UUID> {
}
