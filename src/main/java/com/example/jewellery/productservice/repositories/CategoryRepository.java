package com.example.jewellery.productservice.repositories;

import com.example.jewellery.productservice.entities.Category;
import com.example.jewellery.productservice.entities.Material;
import java.util.List;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<Category, UUID> {

    Category findCategoryByName(String name);
}
