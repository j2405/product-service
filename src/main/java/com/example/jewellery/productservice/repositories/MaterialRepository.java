package com.example.jewellery.productservice.repositories;

import com.example.jewellery.productservice.entities.Material;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MaterialRepository extends JpaRepository<Material, UUID> {

}
