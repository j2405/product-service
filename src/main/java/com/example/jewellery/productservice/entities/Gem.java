package com.example.jewellery.productservice.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.UUID;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table
public class Gem {
    @Id
    @Type(type = "pg-uuid")
    @GeneratedValue
    private UUID id;

    @Column(nullable = false)
    private String name;
}
