package com.example.jewellery.productservice.entities;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table
public class Product implements Serializable {
  @Id
  @Type(type = "pg-uuid")
  @GeneratedValue
  private UUID id;

  @Column(nullable = false)
  private String name;

  @Column(nullable = false)
  private double price;

  @Column(nullable = false)
  private int quantity;

  @Column(nullable = false)
  private LocalDateTime dateOfAddition;

  @ManyToOne(optional = false)
  private Category category;

  @ManyToMany
  private List<Material> material;

  @ManyToMany
  private List<Gem> gems;

  @OneToMany
  private List<Image> images;
}
