package com.example.jewellery.productservice.services.interfaces;

import com.example.jewellery.productservice.dtos.CategoryDto;
import com.example.jewellery.productservice.dtos.ParentChildCategoryDto;
import com.example.jewellery.productservice.entities.Category;
import com.example.jewellery.productservice.entities.Material;
import java.util.List;
import java.util.UUID;

public interface CategoryService {
  void createCategory(String name);

  List<Category> getAllCategories();

  Category getCategory(String name);

}
