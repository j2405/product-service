package com.example.jewellery.productservice.services.interfaces;

import com.example.jewellery.productservice.entities.Category;
import com.example.jewellery.productservice.entities.Gem;

import java.util.List;

public interface GemService {

    void createGem(String name);

    List<Gem> getAll();
}
