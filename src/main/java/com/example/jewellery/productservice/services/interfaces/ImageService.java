package com.example.jewellery.productservice.services.interfaces;

import com.example.jewellery.productservice.entities.Image;
import java.io.IOException;
import org.springframework.web.multipart.MultipartFile;

public interface ImageService {

  Image createImage(String name,MultipartFile file) throws IOException;

}
