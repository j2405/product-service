package com.example.jewellery.productservice.services;

import com.example.jewellery.productservice.adapters.CategoryAdapter;
import com.example.jewellery.productservice.dtos.CategoryDto;
import com.example.jewellery.productservice.dtos.ParentChildCategoryDto;
import com.example.jewellery.productservice.entities.Category;
import com.example.jewellery.productservice.repositories.CategoryRepository;
import com.example.jewellery.productservice.services.interfaces.CategoryService;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CategoryServiceImpl implements CategoryService {

  private final CategoryRepository categoryRepository;

  @Override
  public void createCategory(String name) {
    var newCategory = new Category();
    newCategory.setId(UUID.randomUUID());
    newCategory.setName(name);

    categoryRepository.save(newCategory);
  }

  @Override
  public List<Category> getAllCategories() {
    return categoryRepository.findAll();
  }

  @Override
  public Category getCategory(String name) {
    return categoryRepository.findCategoryByName(name);
  }

}
