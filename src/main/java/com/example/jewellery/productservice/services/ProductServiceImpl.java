package com.example.jewellery.productservice.services;

import com.example.jewellery.productservice.dtos.CartProductDto;
import com.example.jewellery.productservice.dtos.CartProductsDto;
import com.example.jewellery.productservice.dtos.CreateProductDto;
import com.example.jewellery.productservice.dtos.MinMaxProductPriceRangeDto;
import com.example.jewellery.productservice.dtos.ProductOptionsDto;
import com.example.jewellery.productservice.entities.Product;
import com.example.jewellery.productservice.repositories.CategoryRepository;
import com.example.jewellery.productservice.repositories.GemRepository;
import com.example.jewellery.productservice.repositories.MaterialRepository;
import com.example.jewellery.productservice.repositories.ProductRepository;
import com.example.jewellery.productservice.services.interfaces.ImageService;
import com.example.jewellery.productservice.services.interfaces.ProductService;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.Projections;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

  private final ProductRepository productRepository;
  private final CategoryRepository categoryRepository;
  private final MaterialRepository materialRepository;
  private final GemRepository gemRepository;
  private final ImageService imageService;
  private final EntityManager entityManager;

  @Override
  public void createProduct(CreateProductDto createProductDto,
      MultipartFile[] files) throws IOException {

    var newProduct = Product.builder().id(UUID.randomUUID())
        .name(createProductDto.getName()).price(createProductDto.getPrice())
        .quantity(createProductDto.getQuantity()).dateOfAddition(LocalDateTime.now())
        .images(new ArrayList<>())
        .material(new ArrayList<>())
            .gems(new ArrayList<>()).build();

    newProduct.setCategory(categoryRepository.getById(createProductDto.getCategoryId()));
    var productMaterials = newProduct.getMaterial();
    createProductDto.getMaterials()
        .forEach(materialId -> productMaterials.add(materialRepository.getById(materialId)));
    var productGems = newProduct.getGems();
    createProductDto.getGems()
            .forEach(gemId -> productGems.add(gemRepository.getById(gemId)));

    for (MultipartFile file : files) {
      newProduct.getImages().add(imageService.createImage(createProductDto.getName(), file));
    }
    productRepository.save(newProduct);
  }

  @Override
  public Page<Product> getAllProducts(ProductOptionsDto productOptionsDto) {
    var sorting = Sort.by(Direction.fromString(productOptionsDto.getSortDirection()),
        productOptionsDto.getSortColumn());
    var pageRequest = PageRequest
        .of(productOptionsDto.getPage(), productOptionsDto.getPageSize(), sorting);
    return productRepository.findAll((root, query, builder) ->
            builder.and(getProductPaginationPredicates(productOptionsDto, root, builder)
                .toArray(new Predicate[]{}))
        , pageRequest);
  }

   List<Predicate> getProductPaginationPredicates(ProductOptionsDto productOptionsDto,
      javax.persistence.criteria.Root root,
      javax.persistence.criteria.CriteriaBuilder builder) {
    List<Predicate> predicates = new ArrayList<>();
    if (productOptionsDto.getCategoryId() != null) {
      var category = root.join("category");
      predicates.add(builder.equal(category.get("id"), productOptionsDto.getCategoryId()));
    }
    if (productOptionsDto.getGem() !=null){
      var gems = root.join("gems");
      predicates.add(builder.equal(gems.get("id"), productOptionsDto.getGem()));
    }
    if (productOptionsDto.getMaterial() !=null){
      var material = root.join("material");
      predicates.add(builder.equal(material.get("id"), productOptionsDto.getMaterial()));
    }
    if (StringUtils.isNotBlank(productOptionsDto.getName())) {
      predicates.add(builder.like(root.get("name"), "%" + productOptionsDto.getName() + "%"));
    }

    var minPrice = productOptionsDto.getMinPrice();
    var maxPrice = productOptionsDto.getMaxPrice();
    var pricePath = root.get("price");
    if (minPrice != null) {
      predicates.add(builder.greaterThanOrEqualTo(pricePath, minPrice));
    }
    if (maxPrice != null) {
      predicates.add(builder.lessThanOrEqualTo(pricePath, maxPrice));
    }

    return predicates;
  }

  @Override
  public Product get(UUID productId) {
    return productRepository.getById(productId);
  }

  @Override
  public CartProductsDto getCartProductsInfo(List<UUID> productIds) {
    var productsInCart = productRepository.findAllById(productIds.stream().distinct().collect(
        Collectors.toList()));

    var cartProductsDto = new CartProductsDto();
    var total = 0.0;
    for (Product product : productsInCart) {
      var duplicateCount = Collections.frequency(productIds, product.getId());

      var cartProduct = CartProductDto.builder().id(product.getId()).productName(product.getName())
          .count(duplicateCount).price(product.getPrice()).imageUrls(
              product.getImages().stream().map(image -> image.getImageUrl())
                  .collect(Collectors.toList())).build();
      cartProductsDto.getProducts().add(cartProduct);

      total += product.getPrice() * duplicateCount;
    }

    cartProductsDto.setTotal(total);
    return cartProductsDto;
  }

  @Override
  public MinMaxProductPriceRangeDto getMinMaxPriceRange(ProductOptionsDto productOptionsDto) {
    CriteriaBuilder criteriaBuilder = this.entityManager.getCriteriaBuilder();
    CriteriaQuery<Product> query = criteriaBuilder.createQuery(Product.class);
    Root<Product> product = query.from(Product.class);

    query.where(criteriaBuilder.and(getProductPaginationPredicates(productOptionsDto, product, criteriaBuilder).toArray(new Predicate[0])));
    query.select(product);

   var resultList = entityManager.createQuery(query).getResultList();
    return new MinMaxProductPriceRangeDto(resultList.stream().map(p->p.getPrice()).mapToDouble(v->v).min().orElse(0.0),resultList.stream().map(p->p.getPrice()).mapToDouble(v->v).max().orElse(0.0));
  }
}
