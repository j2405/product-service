package com.example.jewellery.productservice.services;

import com.example.jewellery.productservice.entities.Category;
import com.example.jewellery.productservice.entities.Gem;
import com.example.jewellery.productservice.repositories.CategoryRepository;
import com.example.jewellery.productservice.repositories.GemRepository;
import com.example.jewellery.productservice.services.interfaces.GemService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class GemServiceImpl implements GemService {

    private final GemRepository gemRepository;

    @Override
    public void createGem(String name) {
        var newGem = new Gem();
        newGem.setId(UUID.randomUUID());
        newGem.setName(name);

        gemRepository.save(newGem);
    }

    @Override
    public List<Gem> getAll() {
        return gemRepository.findAll();
    }
}
