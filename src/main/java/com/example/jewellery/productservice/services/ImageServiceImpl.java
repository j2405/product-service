package com.example.jewellery.productservice.services;

import com.amazonaws.services.s3.AmazonS3;
import com.example.jewellery.productservice.entities.Image;
import com.example.jewellery.productservice.repositories.ImageRepository;
import com.example.jewellery.productservice.services.interfaces.ImageService;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
@RequiredArgsConstructor
public class ImageServiceImpl implements ImageService {

  private final ImageRepository imageRepository;
  private final AmazonS3 s3Client;
  private static final String IMAGE_BUCKET_NAME = "jewellery-project";

  @Override
  public Image createImage(String name, MultipartFile file) throws IOException {
    var newName = name+file.getOriginalFilename();
    var newFile =  new File(newName);
    FileUtils.copyInputStreamToFile(file.getInputStream(), newFile);
    s3Client.putObject(
        IMAGE_BUCKET_NAME,
        newName,newFile
      );

      newFile.delete();

    var newImage = Image.builder().id(UUID.randomUUID()).imageUrl(
        s3Client.getObject(IMAGE_BUCKET_NAME, newName).getObjectContent().getHttpRequest()
            .getURI().toString()).build();

    return imageRepository.save(newImage);
  }
}
