package com.example.jewellery.productservice.services.interfaces;

import com.example.jewellery.productservice.dtos.CartProductsDto;
import com.example.jewellery.productservice.dtos.CreateProductDto;
import com.example.jewellery.productservice.dtos.MinMaxProductPriceRangeDto;
import com.example.jewellery.productservice.dtos.ProductOptionsDto;
import com.example.jewellery.productservice.entities.Product;
import java.io.IOException;
import java.util.List;
import java.util.UUID;
import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;

public interface ProductService {

  void createProduct(CreateProductDto createProductDto,
      MultipartFile[] files) throws IOException;

  Page<Product> getAllProducts(ProductOptionsDto productOptionsDto);

  Product get(UUID productId);

  CartProductsDto getCartProductsInfo(List<UUID> productIds);

  MinMaxProductPriceRangeDto getMinMaxPriceRange(ProductOptionsDto productOptionsDto);
}
