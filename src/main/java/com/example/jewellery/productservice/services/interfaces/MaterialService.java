package com.example.jewellery.productservice.services.interfaces;

import com.example.jewellery.productservice.entities.Material;
import java.util.List;

public interface MaterialService {

  void createMaterial(String name, String color);

  List<Material> getAllMaterials();

}
