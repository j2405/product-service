package com.example.jewellery.productservice.services;

import com.example.jewellery.productservice.entities.Material;
import com.example.jewellery.productservice.repositories.MaterialRepository;
import com.example.jewellery.productservice.services.interfaces.MaterialService;
import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MaterialServiceImpl implements MaterialService {

  private final MaterialRepository materialRepository;

  @Override
  public void createMaterial(String name, String color) {
    var material = Material.builder().id(UUID.randomUUID()).name(name).color(color).build();
    materialRepository.save(material);
  }

  @Override
  public List<Material> getAllMaterials() {
    return materialRepository.findAll();
  }
}
