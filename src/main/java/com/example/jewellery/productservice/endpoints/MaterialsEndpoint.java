package com.example.jewellery.productservice.endpoints;

import com.example.jewellery.productservice.adapters.MaterialAdapter;
import com.example.jewellery.productservice.dtos.CreateMaterialDto;
import com.example.jewellery.productservice.dtos.MaterialDto;
import com.example.jewellery.productservice.services.interfaces.MaterialService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/materials")
public class MaterialsEndpoint {

  private final MaterialService materialService;

  @PostMapping
  public ResponseEntity<Void> createMaterial(@RequestBody CreateMaterialDto createMaterialDto){
    materialService.createMaterial(createMaterialDto.getName(), createMaterialDto.getColor());
    return ResponseEntity.ok().build();
  }

  @GetMapping
  public ResponseEntity<List<MaterialDto>> getAll(){
    return ResponseEntity.ok().body(MaterialAdapter.convertToDto(materialService.getAllMaterials()));
  }
}
