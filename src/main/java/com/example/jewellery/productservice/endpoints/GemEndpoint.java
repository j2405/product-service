package com.example.jewellery.productservice.endpoints;

import com.example.jewellery.productservice.adapters.CategoryAdapter;
import com.example.jewellery.productservice.adapters.GemAdapter;
import com.example.jewellery.productservice.dtos.CategoryDto;
import com.example.jewellery.productservice.dtos.CreateCategoryDto;
import com.example.jewellery.productservice.dtos.GemDto;
import com.example.jewellery.productservice.services.GemServiceImpl;
import com.example.jewellery.productservice.services.interfaces.CategoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/gems")
public class GemEndpoint {

    private final GemServiceImpl gemServiceImpl;

    @PostMapping
    public ResponseEntity<Void> createGem(@RequestBody CreateCategoryDto createCategoryDto){
        gemServiceImpl.createGem(createCategoryDto.getName());
        return ResponseEntity.ok().build();
    }

    @GetMapping
    public ResponseEntity<List<GemDto>> getAll(){
        return ResponseEntity.ok().body(GemAdapter.convertToDto(gemServiceImpl.getAll()));
    }
}
