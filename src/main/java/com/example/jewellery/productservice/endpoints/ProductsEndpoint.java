package com.example.jewellery.productservice.endpoints;

import com.example.jewellery.productservice.adapters.MaterialAdapter;
import com.example.jewellery.productservice.adapters.ProductAdapter;
import com.example.jewellery.productservice.dtos.CartProductsDto;
import com.example.jewellery.productservice.dtos.CreateProductDto;
import com.example.jewellery.productservice.dtos.MaterialDto;
import com.example.jewellery.productservice.dtos.MinMaxProductPriceRangeDto;
import com.example.jewellery.productservice.dtos.PriceRange;
import com.example.jewellery.productservice.dtos.ProductDto;
import com.example.jewellery.productservice.dtos.ProductOptionsDto;
import com.example.jewellery.productservice.dtos.ProductsPage;
import com.example.jewellery.productservice.services.interfaces.ProductService;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequiredArgsConstructor
@RequestMapping("/products")
public class ProductsEndpoint {

  private final ProductService productService;

  @PostMapping( consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
  public ResponseEntity<Void> createProduct(CreateProductDto createProductDto, MultipartFile[] files)
      throws IOException {
    productService.createProduct(createProductDto,  files);
    return ResponseEntity.ok().build();

  }

  @GetMapping
  public ResponseEntity<ProductsPage> getAll(ProductOptionsDto productOptionsDto){
    return ResponseEntity.ok().body(ProductAdapter.convertToPage(productService.getAllProducts(productOptionsDto)));
  }

  @GetMapping("/price-range")
  public ResponseEntity<MinMaxProductPriceRangeDto> getProductRange(ProductOptionsDto productOptionsDto){
    return ResponseEntity.ok().body(productService.getMinMaxPriceRange(productOptionsDto));
  }

  @GetMapping("/{productId}")
  public ResponseEntity<ProductDto> getProduct(@PathVariable  UUID productId){
    return ResponseEntity.ok().body(ProductAdapter.convertToDto(productService.get(productId)));
  }

  @GetMapping("/cart")
  public ResponseEntity<CartProductsDto> getProduct(@RequestParam UUID[] productIds){
    return ResponseEntity.ok().body(productService.getCartProductsInfo(Arrays.asList(productIds)));
  }

  @GetMapping("/test")
  public String test() {

    return "test";
  }
}
