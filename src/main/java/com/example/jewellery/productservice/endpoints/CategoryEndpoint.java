package com.example.jewellery.productservice.endpoints;

import com.example.jewellery.productservice.adapters.CategoryAdapter;
import com.example.jewellery.productservice.adapters.MaterialAdapter;
import com.example.jewellery.productservice.dtos.CategoryDto;
import com.example.jewellery.productservice.dtos.CreateCategoryDto;
import com.example.jewellery.productservice.dtos.MaterialDto;
import com.example.jewellery.productservice.dtos.ParentChildCategoryDto;
import com.example.jewellery.productservice.services.interfaces.CategoryService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/categories")
public class CategoryEndpoint {

  private final CategoryService categoryService;

  @PostMapping
  public ResponseEntity<Void> createCategory(@RequestBody CreateCategoryDto createCategoryDto){
    categoryService.createCategory(createCategoryDto.getName());
    return ResponseEntity.ok().build();
  }

  @GetMapping
  public ResponseEntity<List<CategoryDto>> getAll(){
    return ResponseEntity.ok().body(CategoryAdapter.convertToDto(categoryService.getAllCategories()));
  }

  @GetMapping("/{name}")
  public ResponseEntity<CategoryDto> getByName(@PathVariable String name){
    return ResponseEntity.ok().body(CategoryAdapter.convertToDto(categoryService.getCategory(name)));
  }

}
