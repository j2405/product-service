package com.example.jewellery.productservice.dtos;

import java.util.List;
import java.util.UUID;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class CreateProductDto {
  private String name;

  private double price;

  private int quantity;

  private UUID categoryId;

  private List<UUID> materials;

  private List<UUID> gems;
}
