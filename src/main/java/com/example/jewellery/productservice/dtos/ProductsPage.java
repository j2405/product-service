package com.example.jewellery.productservice.dtos;

import java.util.List;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ProductsPage {
private List<ProductDto> products;
private boolean hasNext;
}
