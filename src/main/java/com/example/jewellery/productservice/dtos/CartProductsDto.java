package com.example.jewellery.productservice.dtos;

import java.util.ArrayList;
import java.util.List;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CartProductsDto {
  private List<CartProductDto> products = new ArrayList<>();
  private double total;
}
