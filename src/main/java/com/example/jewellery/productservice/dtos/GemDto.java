package com.example.jewellery.productservice.dtos;

import lombok.Builder;
import lombok.Data;

import java.util.UUID;
@Data
@Builder
public class GemDto {
    private String name;
    private UUID id;
}
