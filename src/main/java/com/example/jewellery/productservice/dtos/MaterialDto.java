package com.example.jewellery.productservice.dtos;

import java.util.UUID;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class MaterialDto {
  private UUID id;
  private String name;
  private String color;
}
