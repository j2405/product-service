package com.example.jewellery.productservice.dtos;

import java.util.UUID;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CategoryDto {
  private String name;
  private UUID id;
}
