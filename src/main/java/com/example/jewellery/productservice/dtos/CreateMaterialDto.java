package com.example.jewellery.productservice.dtos;

import lombok.Data;

@Data
public class CreateMaterialDto {
  private String name;
  private String color;
}
