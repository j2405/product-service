package com.example.jewellery.productservice.dtos;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ProductDto {
  private UUID id;
  private String name;
  private double price;
  private int quantity;
  private List<MaterialDto> materials;
  private List<GemDto> gems;
  private CategoryDto category;
  private LocalDateTime dateOfAddition;
  private List<String> imageUrls;
}
