package com.example.jewellery.productservice.dtos;

import java.util.UUID;
import lombok.Data;

@Data
public class ProductOptionsDto {
  private String name;
  private Integer pageSize;
  private int page;
  private UUID categoryId;
  private String sortColumn;
  private String sortDirection;
  private Integer minPrice;
  private Integer maxPrice;
  private UUID gem;
  private UUID material;
}
