package com.example.jewellery.productservice.dtos;

import java.util.UUID;
import lombok.Data;

@Data
public class CreateCategoryDto {
  private String name;
}
