package com.example.jewellery.productservice.dtos;

import java.util.List;
import java.util.UUID;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CartProductDto {
  private UUID id;
  private String productName;
  private int count;
  private double price;
  private List<String> imageUrls;
}
