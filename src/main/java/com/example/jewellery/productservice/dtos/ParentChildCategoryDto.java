package com.example.jewellery.productservice.dtos;

import java.util.List;
import java.util.UUID;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ParentChildCategoryDto {
  private String name;
  private UUID id;
  private List<CategoryDto> childCategories;
}
