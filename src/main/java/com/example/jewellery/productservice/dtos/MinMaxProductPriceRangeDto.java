package com.example.jewellery.productservice.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
public class MinMaxProductPriceRangeDto {

  private double minPrice;
  private double maxPrice;

}
