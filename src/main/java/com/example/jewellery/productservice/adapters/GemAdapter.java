package com.example.jewellery.productservice.adapters;

import com.example.jewellery.productservice.dtos.CategoryDto;
import com.example.jewellery.productservice.dtos.GemDto;
import com.example.jewellery.productservice.entities.Category;
import com.example.jewellery.productservice.entities.Gem;

import java.util.List;
import java.util.stream.Collectors;

public class GemAdapter {

    public static List<GemDto> convertToDto(List<Gem> gems) {
        return gems.stream().map(gem -> convertToDto(gem)).collect(Collectors.toList());
    }

    public static GemDto convertToDto(Gem category) {
        return GemDto.builder().id(category.getId()).name(category.getName()).build();
    }
}
