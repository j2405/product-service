package com.example.jewellery.productservice.adapters;

import com.example.jewellery.productservice.dtos.MaterialDto;
import com.example.jewellery.productservice.entities.Material;
import java.util.List;
import java.util.stream.Collectors;

public class MaterialAdapter {

  public static List<MaterialDto> convertToDto(List<Material> materials) {
    return materials.stream().map(material -> convertToDto(material)).collect(Collectors.toList());
  }

  private static MaterialDto convertToDto(Material material) {
    return MaterialDto.builder().id(material.getId()).color(material.getColor()).name(material.getName()).build();
  }


}
