package com.example.jewellery.productservice.adapters;

import com.example.jewellery.productservice.dtos.CartProductsDto;
import com.example.jewellery.productservice.dtos.CategoryDto;
import com.example.jewellery.productservice.dtos.ProductDto;
import com.example.jewellery.productservice.dtos.ProductsPage;
import com.example.jewellery.productservice.entities.Category;
import com.example.jewellery.productservice.entities.Product;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.data.domain.Page;

public class ProductAdapter {

  public static List<ProductDto> convertToDto(List<Product> products) {
    return products.stream().map(product -> convertToDto(product)).collect(Collectors.toList());
  }

  public static ProductDto convertToDto(Product product) {
    return ProductDto.builder().id(product.getId()).name(product.getName())
        .category(CategoryAdapter.convertToDto(product.getCategory()))
        .materials(MaterialAdapter.convertToDto(product.getMaterial())).price(product.getPrice())
            .gems(GemAdapter.convertToDto(product.getGems()))
        .dateOfAddition(product.getDateOfAddition())
        .quantity(product.getQuantity())
        .imageUrls(product.getImages().stream().map(image->image.getImageUrl()).collect(Collectors.toList()))
        .build();
  }

  public static ProductsPage convertToPage(Page<Product> domainPage) {

    return ProductsPage.builder().products(convertToDto(domainPage.getContent()))
        .hasNext(domainPage.hasNext()).build();
  }

}
