package com.example.jewellery.productservice.adapters;

import com.example.jewellery.productservice.dtos.CategoryDto;
import com.example.jewellery.productservice.dtos.MaterialDto;
import com.example.jewellery.productservice.entities.Category;
import com.example.jewellery.productservice.entities.Material;
import java.util.List;
import java.util.stream.Collectors;

public class CategoryAdapter {

  public static List<CategoryDto> convertToDto(List<Category> categories) {
    return categories.stream().map(category -> convertToDto(category)).collect(Collectors.toList());
  }

  public static CategoryDto convertToDto(Category category) {
    return CategoryDto.builder().id(category.getId()).name(category.getName()).build();
  }
}
