INSERT INTO public.gem
(id, name)
VALUES(gen_random_uuid (), 'Topaz');

INSERT INTO public.gem
(id, name)
VALUES(gen_random_uuid (), 'Opal');

INSERT INTO public.gem
(id, name)
VALUES(gen_random_uuid (), 'Ruby');

INSERT INTO public.gem
(id, name)
VALUES(gen_random_uuid (), 'Emerald');

INSERT INTO public.gem
(id, name)
VALUES(gen_random_uuid (), 'Sapphire');

INSERT INTO public.gem
(id, name)
VALUES(gen_random_uuid (), 'Amethyst');

INSERT INTO public.gem
(id, name)
VALUES(gen_random_uuid (), 'Citrine');