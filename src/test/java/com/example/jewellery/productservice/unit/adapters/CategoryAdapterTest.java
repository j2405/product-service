package com.example.jewellery.productservice.unit.adapters;

import com.example.jewellery.productservice.adapters.CategoryAdapter;
import com.example.jewellery.productservice.dtos.CategoryDto;
import com.example.jewellery.productservice.entities.Category;
import java.util.UUID;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

public class CategoryAdapterTest {
  @Test
  public void testCategoryConversion() {
    var newUuid= UUID.randomUUID();
    var categoryDto = CategoryAdapter.convertToDto(
        Category.builder().id(newUuid).name("test name")
            .build());
    Assertions.assertThat(categoryDto)
        .isEqualTo(
            CategoryDto.builder().id(newUuid).name("test name").build());
  }
}
