package com.example.jewellery.productservice.unit.adapters;

import com.example.jewellery.productservice.adapters.GemAdapter;
import com.example.jewellery.productservice.dtos.GemDto;
import com.example.jewellery.productservice.entities.Gem;
import java.util.UUID;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

public class GemAdapterTest {

  @Test
  public void testGemConversion() {
    var newUuid= UUID.randomUUID();
    var gemDto = GemAdapter.convertToDto(
        Gem.builder().id(newUuid).name("test name")
            .build());
    Assertions.assertThat(gemDto)
        .isEqualTo(
            GemDto.builder().id(newUuid).name("test name").build());
  }
}
