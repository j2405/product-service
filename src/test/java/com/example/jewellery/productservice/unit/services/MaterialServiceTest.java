package com.example.jewellery.productservice.unit.services;

import com.example.jewellery.productservice.entities.Material;
import com.example.jewellery.productservice.repositories.MaterialRepository;
import com.example.jewellery.productservice.services.MaterialServiceImpl;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class MaterialServiceTest {

  @Mock
  private MaterialRepository materialRepository;

  @InjectMocks
  private MaterialServiceImpl materialService;


  @Test
  public void testCreateCategory() {

    Mockito.when(materialRepository.save(Mockito.any())).thenReturn(Mockito.any());

    materialService.createMaterial("material name","#fff");

    final ArgumentCaptor<Material> captor = ArgumentCaptor.forClass(Material.class);
    Mockito.verify(materialRepository).save(captor.capture());
    var saveCalledWith = captor.getValue();

    Assertions.assertThat(saveCalledWith).isNotNull().usingRecursiveComparison()
        .ignoringFields("id").isEqualTo(Material.builder().name("material name").color("#fff").build());
  }

}
