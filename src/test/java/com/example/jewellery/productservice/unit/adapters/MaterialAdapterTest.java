package com.example.jewellery.productservice.unit.adapters;

import com.example.jewellery.productservice.adapters.GemAdapter;
import com.example.jewellery.productservice.adapters.MaterialAdapter;
import com.example.jewellery.productservice.dtos.GemDto;
import com.example.jewellery.productservice.dtos.MaterialDto;
import com.example.jewellery.productservice.entities.Gem;
import com.example.jewellery.productservice.entities.Material;
import java.util.Arrays;
import java.util.UUID;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

public class MaterialAdapterTest {


  @Test
  public void testMaterialConversion() {
    var newUuid= UUID.randomUUID();
    var materialDto = MaterialAdapter.convertToDto(
        Arrays.asList(        Material.builder().id(newUuid).name("test name").color("#fff")
            .build()));
    Assertions.assertThat(materialDto)
        .isEqualTo(
            Arrays.asList(MaterialDto.builder().id(newUuid).name("test name").color("#fff").build()));
  }
}
