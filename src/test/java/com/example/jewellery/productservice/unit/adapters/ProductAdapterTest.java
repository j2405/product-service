package com.example.jewellery.productservice.unit.adapters;

import com.example.jewellery.productservice.adapters.MaterialAdapter;
import com.example.jewellery.productservice.adapters.ProductAdapter;
import com.example.jewellery.productservice.dtos.CategoryDto;
import com.example.jewellery.productservice.dtos.MaterialDto;
import com.example.jewellery.productservice.dtos.ProductDto;
import com.example.jewellery.productservice.entities.Category;
import com.example.jewellery.productservice.entities.Material;
import com.example.jewellery.productservice.entities.Product;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.UUID;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

public class ProductAdapterTest {
  @Test
  public void testProductConversion() {
    var newUuid= UUID.randomUUID();
    var materialDto = ProductAdapter.convertToDto(
        Arrays.asList(Product.builder()
            .id(newUuid)
            .name("test name")
            .price(20.0)
            .images(new ArrayList<>())
            .quantity(2).material(new ArrayList<>())
            .category(Category.builder().id(newUuid).name("test name").build())
            .gems(new ArrayList<>())
            .build()));
    Assertions.assertThat(materialDto)
        .isEqualTo(
            Arrays.asList(ProductDto.builder().id(newUuid)       .name("test name")
                .price(20.0)
                .imageUrls(new ArrayList<>())
                .quantity(2)
                .materials(new ArrayList<>())
                .category(CategoryDto.builder().id(newUuid).name("test name").build())
                .gems(new ArrayList<>()).build()));
  }
}
