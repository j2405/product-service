package com.example.jewellery.productservice.unit.services;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.example.jewellery.productservice.entities.Gem;
import com.example.jewellery.productservice.entities.Image;
import com.example.jewellery.productservice.repositories.ImageRepository;
import com.example.jewellery.productservice.services.ImageServiceImpl;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import org.apache.commons.io.FileUtils;
import org.apache.http.client.methods.HttpGet;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;
import org.springframework.web.multipart.MultipartFile;

@ExtendWith(MockitoExtension.class)
public class ImageServiceTest {

  @Mock
  private ImageRepository imageRepository;

  @Mock
  private AmazonS3 s3Client;


  @InjectMocks
  private ImageServiceImpl imageService;

  @Test
  public void testCreateImage() throws IOException {

    var httpRequest = new HttpGet();
    httpRequest.setURI(URI.create("http://localhost:test"));
    var s3ObjectContent = Mockito.mock(S3ObjectInputStream.class);
    when(s3ObjectContent.getHttpRequest()).thenReturn(httpRequest);
    var s3Object = new S3Object();
    s3Object.setObjectContent(s3ObjectContent);
    when(s3Client.getObject("jewellery-project", "newImageCoolName")).thenReturn(s3Object);

    var fileUtilsMock = Mockito.mockStatic(FileUtils.class);
    fileUtilsMock.when(() -> FileUtils.copyFile(Mockito.any(File.class), Mockito.any(File.class)))
        .thenAnswer((Answer<Void>) invocation -> null);

    when(imageRepository.save(any())).thenReturn(new Image());

    var mockFile = Mockito.mock(MultipartFile.class);
    when(mockFile.getOriginalFilename()).thenReturn("CoolName");

    when(s3Client.putObject(Mockito.any(), Mockito.anyString(), any(File.class))).thenReturn(null);

    imageService.createImage("newImage", mockFile);

    final ArgumentCaptor<Image> captor = ArgumentCaptor.forClass(Image.class);
    Mockito.verify(imageRepository).save(captor.capture());
    var saveCalledWith = captor.getValue();

    Assertions.assertThat(saveCalledWith).isNotNull().usingRecursiveComparison()
        .ignoringFields("id").isEqualTo(Image.builder().imageUrl("http://localhost:test").build());
  }
}
