package com.example.jewellery.productservice.unit.services;


import static org.mockito.Mockito.when;

import com.example.jewellery.productservice.dtos.CartProductDto;
import com.example.jewellery.productservice.dtos.CartProductsDto;
import com.example.jewellery.productservice.dtos.CreateProductDto;
import com.example.jewellery.productservice.entities.Category;
import com.example.jewellery.productservice.entities.Product;
import com.example.jewellery.productservice.repositories.CategoryRepository;
import com.example.jewellery.productservice.repositories.ProductRepository;
import com.example.jewellery.productservice.services.ProductServiceImpl;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.multipart.MultipartFile;

@ExtendWith(MockitoExtension.class)
public class ProductServiceTest {

  @Mock
  private ProductRepository productRepository;

  @Mock
  private CategoryRepository categoryRepository;

  @InjectMocks
  private ProductServiceImpl productService;


  @Test
  public void testCreateProduct() throws IOException {

    when(productRepository.save(Mockito.any())).thenReturn(new Product());
    when(categoryRepository.getById(Mockito.any(UUID.class))).thenReturn(new Category());

    productService.createProduct(prepareCreateProductDto(), new MultipartFile[0]);

    Mockito.verify(productRepository).save(Mockito.any());
  }

  @Test
  public void testGetCartProducts() {

    var newId = UUID.randomUUID();
    var products = Arrays.asList(
        Product.builder().id(newId).name("product name").price(20.5).images(new ArrayList<>())
            .build());
    when(productRepository.findAllById(Mockito.any(List.class))).thenReturn(products);

    var cartProducts = productService.getCartProductsInfo(Arrays.asList(newId));

    Mockito.verify(productRepository).findAllById(Mockito.any(List.class));

    var expectedCartProductsDto = new CartProductsDto();
    expectedCartProductsDto.setProducts(Arrays.asList(CartProductDto.builder().id(newId)
        .productName("product name").count(1).imageUrls(new ArrayList<>()).price(20.5).build()));
    expectedCartProductsDto.setTotal(20.5);
    Assertions.assertThat(cartProducts).isEqualTo(expectedCartProductsDto);


  }


  private CreateProductDto prepareCreateProductDto() {
    var productDto = new CreateProductDto();
    productDto.setName("product name");
    productDto.setGems(new ArrayList<>());
    productDto.setPrice(25.5);
    productDto.setCategoryId(UUID.randomUUID());
    productDto.setMaterials(new ArrayList<>());
    productDto.setQuantity(5);
    return productDto;
  }
}
