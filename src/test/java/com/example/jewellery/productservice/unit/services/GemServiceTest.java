package com.example.jewellery.productservice.unit.services;

import com.example.jewellery.productservice.entities.Gem;
import com.example.jewellery.productservice.repositories.GemRepository;
import com.example.jewellery.productservice.services.GemServiceImpl;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class GemServiceTest {

  @Mock
  private GemRepository gemRepository;

  @InjectMocks
  private GemServiceImpl gemService;


  @Test
  public void testCreateGem() {


    Mockito.when(gemRepository.save(Mockito.any())).thenReturn(Mockito.any());

    gemService.createGem("gem name");


    final ArgumentCaptor<Gem> captor = ArgumentCaptor.forClass(Gem.class);
    Mockito.verify(gemRepository).save(captor.capture());
   var saveCalledWith = captor.getValue();

    Assertions.assertThat(saveCalledWith).isNotNull().usingRecursiveComparison().ignoringFields("id").isEqualTo(Gem.builder().name("gem name").build());
  }
}
