package com.example.jewellery.productservice.unit.services;

import com.example.jewellery.productservice.entities.Category;
import com.example.jewellery.productservice.repositories.CategoryRepository;
import com.example.jewellery.productservice.services.CategoryServiceImpl;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class CategoryServiceTest {

  @Mock
  private CategoryRepository categoryRepository;

  @InjectMocks
  private CategoryServiceImpl categoryService;


  @Test
  public void testCreateCategory() {

    Mockito.when(categoryRepository.save(Mockito.any())).thenReturn(Mockito.any());

    categoryService.createCategory("new category");

    final ArgumentCaptor<Category> captor = ArgumentCaptor.forClass(Category.class);
    Mockito.verify(categoryRepository).save(captor.capture());
    var saveCalledWith = captor.getValue();

    Assertions.assertThat(saveCalledWith).isNotNull().usingRecursiveComparison()
        .ignoringFields("id").isEqualTo(Category.builder().name("new category").build());
  }
}
