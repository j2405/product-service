package com.example.jewellery.productservice.services;

import static org.mockito.Mockito.when;

import com.example.jewellery.productservice.dtos.ProductOptionsDto;
import com.example.jewellery.productservice.entities.Category;
import com.example.jewellery.productservice.entities.Product;
import java.io.IOException;
import java.util.UUID;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.assertj.core.api.Assertions;
import org.hibernate.query.criteria.internal.CriteriaBuilderImpl;
import org.hibernate.query.criteria.internal.path.RootImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.multipart.MultipartFile;

@ExtendWith(MockitoExtension.class)
public class ProductServicePaginationPredicatesTest {

  @InjectMocks
  private ProductServiceImpl productService;


  @Test
  public void testGetAllPredicates() {
    var options = new ProductOptionsDto();
    options.setMinPrice(20);
    options.setMaxPrice(50);
    options.setGem(UUID.randomUUID());
    options.setMaterial(UUID.randomUUID());
    options.setCategoryId(UUID.randomUUID());
    options.setName("test");

    var join = Mockito.mock(Join.class);
    var root = Mockito.mock(Root.class);
    when(root.join(Mockito.anyString())).thenReturn(join);
    var criteriaBuilder = Mockito.mock(CriteriaBuilder.class);

    var predicates = productService.getProductPaginationPredicates(options, root, criteriaBuilder);

    Assertions.assertThat(predicates).hasSize(6);
  }

}
